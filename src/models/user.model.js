const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = new Schema({
    username: {
        type: String,
        required: false
    },
    phoneNumber: {
        type: String,
        required: false
    },
    status: {
        type: Number,
        default: 0
    },
    language: {
        type: String,
        required: true
    }
});

mongoose.model('users', User);