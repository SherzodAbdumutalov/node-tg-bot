process.env.NTBA_FIX_319 = 1;
const TelegramApi = require('node-telegram-bot-api');
const config = require('./config');
const helper = require('./helpers');
const messages = require('./messages');
const sqlite3 = require('sqlite3').verbose();
const kb = require('./keyboardButtons');
const keyboard = require('./keyboard');
const bot = new TelegramApi(config.TOKEN, {polling: true});

helper.logStart();

let db = new sqlite3.Database('users.db', (err) => {
    if (err) {
        return console.error(err.message);
    }
    db.run('CREATE TABLE if not exists users(id INTEGER PRIMARY KEY AUTOINCREMENT, username VARCHAR(50), language VARCHAR(20), phone VARCHAR(20), status BOOLEAN)', function(err, res) {
        if (err) {
            return console.error(err.message);
        }

        console.log('Connected to the SQlite database and created users table');

    });
});


bot.on('message', msg => {
    const text = msg.text;
    const chatId = msg.chat.id;
    const data = {
        username: msg.chat.username
    }
    console.log(chatId)
    switch(text) {
        case kb.home.uz:
            data.language = 'uz';
            updateOrCreateUser(data).then((res) => {
                bot.sendMessage(chatId, messages.uz.inviting, {
                    reply_markup: {
                        keyboard: keyboard.uz.phoneNumber,
                        resize_keyboard: true
                    }
                });
            }).catch((err) => {
                console.log(err.message)
            });

            break;

        case kb.home.ru:      
            data.language = 'ru';
            
            updateOrCreateUser(data).then((res) => {
                bot.sendMessage(chatId, messages.ru.inviting, {
                    reply_markup: {
                        keyboard: keyboard.ru.phoneNumber,
                        resize_keyboard: true
                    }
                });
            }).catch((err) => {
                console.log(err.message)
            });
    
            break;

        case kb.uz.back:
            bot.sendMessage(chatId, messages.uz.choose_language, {
                reply_markup: {
                    keyboard: keyboard.home,
                    resize_keyboard: true
                }
            });
            break;

        case kb.ru.back:
            bot.sendMessage(chatId, messages.ru.choose_language, {
                reply_markup: {
                    keyboard: keyboard.home,
                    resize_keyboard: true
                }
            });
            break;

        case kb.uz.question.yes:
            bot.sendMessage(chatId, messages.uz.address, {
                reply_markup: {
                    hide_keyboard: true
                },
                parse_mode: 'HTML'
            });

            bot.sendLocation(msg.chat.id, 41.341762, 69.333910);

            data.status = 1;
            updateOrCreateUser(data);
            break;

        case kb.uz.question.no:
            bot.sendMessage(chatId, messages.uz.bye, {
                reply_markup: {
                    hide_keyboard: true
                },
                parse_mode: 'HTML'
            });
            data.status = 0;
            updateOrCreateUser(data);
            break;

        case kb.ru.question.yes:
            bot.sendMessage(chatId, messages.ru.address, {
                reply_markup: {
                    hide_keyboard: true
                },
                parse_mode: 'HTML'

            });

            bot.sendLocation(msg.chat.id, 41.341762, 69.333910);

            data.status = 1;
            updateOrCreateUser(data);
            break;

        case kb.ru.question.no:
            bot.sendMessage(chatId, messages.ru.bye, {
                reply_markup: {
                    hide_keyboard: true
                },
                parse_mode: 'HTML'

            });
            data.status = 0;
            updateOrCreateUser(data);
            break;
        case kb.uz.statistics:
            getStatistics().then((res) => {
                let response = '<b>Qatnashuvchilar soni: </b>'+res.count;
                bot.sendMessage(msg.chat.id, response, {
                    reply_markup: {
                        keyboard: keyboard['uz'].statistics,
                        resize_keyboard: true
                    },
                    parse_mode: 'HTML'
                });
            });
            break;

        case kb.uz.phones:
            getPhones().then((users) => {
                let html = '0';
                html = users.map((f, i) => {
                    return `<b>${i+1}) </b>${f.phone}`;
                }).join('\n');
                html = (html === '')?'no numbers':html;
                bot.sendMessage(msg.chat.id, html, {
                    reply_markup: {
                        keyboard: keyboard['uz'].statistics,
                        resize_keyboard: true
                    },
                    parse_mode: 'html'
                });
            });
            break;

        case kb.ru.statistics:
            getStatistics().then((res) => {
                let response = '<b>Количество участников: </b>'+res.count;
                bot.sendMessage(msg.chat.id, response, {
                    reply_markup: {
                        keyboard: keyboard['ru'].statistics,
                        resize_keyboard: true
                    },
                    parse_mode: 'HTML'
                });
            });
            break;
            
        case kb.ru.phones:
            getPhones().then((users) => {
                let html = '0';
                html = users.map((f, i) => {
                    return `<b>${i+1}) </b>${f.phone}`;
                }).join('\n');
                html = (html === '')?'no numbers':html;
                bot.sendMessage(msg.chat.id, html, {
                    reply_markup: {
                        keyboard: keyboard['ru'].statistics,
                        resize_keyboard: true
                    },
                    parse_mode: 'html'
                });
            });
            break;
    }

});

bot.onText(/\/start/, msg => {
    bot.sendMessage(msg.chat.id, messages.greating, {
        reply_markup: {
            keyboard: keyboard.home,
            resize_keyboard: true,
        },
        parse_mode: 'HTML'
    })
});

bot.on('contact', msg => {

    let data = {
        username: msg.chat.username,
        phone: msg.contact.phone_number
    };

    updateOrCreateUser(data).then((res) => {
        getCurrentUserLanguage(data.username).then((user) => {
            if(msg.chat.username === 'sherzodAbdumutalov2' || msg.chat.username === 'abstract_vs') {
                bot.sendMessage(msg.chat.id, 'вы админ', {
                    reply_markup: {
                        keyboard: keyboard[user.language].statistics,
                        resize_keyboard: true
                    }
                });
            }else{
                let text = messages.uz.will_come;
                if(user.language === 'ru') {
                    text = messages.ru.will_come;
                }
    
                bot.sendMessage(msg.chat.id, text, {
                    reply_markup: {
                        keyboard: keyboard[user.language].question,
                        resize_keyboard: true
                    }
                });
            }
        });
    });
})

function updateOrCreateUser(data) {

    return new Promise((resolve, reject) => {
        let result = {
            username: data.username
        };

        if(data.username !== undefined) {
            getCurrentUserLanguage(data.username).then((user) => {
                if(user) {
                    if(data.language !== undefined) {
                        updateUserLanguage(data.username, data.language).then((res) => {
                            result.language = user.language;
                        });
                    }
        
                    if(data.phone !== undefined) {
                        updateUserPhone(data.username, data.phone).then((res) => {
                            result.phone = user.phone;
                        });
                    }
        
                    if(data.status !== undefined) {
                        updateUserStatus(data.username, data.status).then((res) => {
                            result.status = user.status;
                        });
                    }
        
                }else {
                    if(data.status === undefined) {
                        data.status = null;
                    }
                    if(data.phone === undefined) {
                        data.phone = null;
                    }
                    addNewUser(data.username, data.language, data.phone, data.status).then((res) => {
                        result.language = res.language;
                        result.status = res.status;
                        result.phone = res.phone;
                    });
                }
            });
    
            
        }

        resolve(result)
    });
}

function getStatistics() {
    let sql = `select count(id) as allUsers from users where status = ?`;

    return new Promise((resolve, reject) => {

        db.get(sql, [1], function(err, row) {
            if(err) {
                console.log(err.message);
            }
            
            resolve({ count: row.allUsers });
        })
    });
}

function getPhones() {
    let sql = `select username, phone from users where status = ?`;

    return new Promise((resolve, reject) => {

        db.all(sql, [1], function(err, rows) {
            if(err) {
                console.log(err.message);
            }
    
            resolve(rows);
        });
    });
}

function getCurrentUserLanguage(username) {

    return new Promise((resolve, reject) => {
        let sql = `select username, language from users where username = ?`;
        db.get(sql, [username], (err, row) => {
            
            if(err) {
                reject(err.message);
            }

            resolve(row);
        });
    })
}

function addNewUser(username, language, phone, status) {
    let sql = `insert into users(id, username, language, phone, status) values(?,?,?,?,?)`;

    return new Promise((resolve, reject) => {
        db.run(sql, [null, username, language, phone, status], function(err, user) {
            console.log(user)
            if(err) {
                reject(err.message);
            }
    
            console.log('user was added');
    
            resolve({username: username, language: language});
    
        });
    })
}


function updateUserPhone(username, phone) {
    let sql =  `update users set phone = ? where username = ?`;

    return new Promise((resolve, reject) => {
        db.run(sql, [phone, username], function(err) {
            if (err) {
                reject(err.message);
            }
            console.log('user phone was updated');
            resolve({username: username});
        });
    });
}

function updateUserLanguage(username, language) {
    let sql =  `update users set language = ? where username = ?`;

    return new Promise((resolve, reject) => {
        db.run(sql, [language, username], function(err) {
            if (err) {
                reject(err.message);
            }
            console.log('user language was updated');
            resolve({username: username});
        });
    });
}

function updateUserStatus(username, status) {
    let sql =  `update users set status = ? where username = ?`;

    return new Promise((resolve, reject) => {
        db.run(sql, [status, username], function(err) {
            if (err) {
                reject(err.message);
            }
            console.log('user stauts was updated');
            resolve({username: username});
        });
    });
}