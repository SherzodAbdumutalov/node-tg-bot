module.exports = {
    home: {
        uz: 'Õzbek 🇺🇿',
        ru: 'Русский 🇷🇺'
    },
    uz: {
        phoneNumber: 'Raqamni yuboring 📲',
        question: {
            yes: 'Xa 👍',
            no: 'Yo\'q 👎'
        },
        back: 'Ortga 🔙',
        home: 'Tilni tanlang 👅',
        statistics: 'Statistika 📈',
        phones: 'Tel. raqamlarni chiqarish 📞'
    },
    ru: {
        phoneNumber: 'Отправить номер 📲',
        question: {
            yes: 'Да 👍',
            no: 'Нет 👎'
        },
        back: 'Назад 🔙',
        home: 'Выберите язык! 👅',
        statistics: 'Статистика 📈',
        phones: 'Запросить номера 📞'
    }
}