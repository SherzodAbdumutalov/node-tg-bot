const kb = require('./keyboardButtons');
module.exports = {
    home: [
        [kb.home.uz, kb.home.ru]
    ],
    uz: {
        phoneNumber: [
            [{
                text: kb['uz'].phoneNumber,
                request_contact: true
            }], 
            [kb['uz'].back]
        ],
        question: [
            [kb['uz'].question.yes, kb['uz'].question.no],
        ],
        statistics: [
            [kb['uz'].statistics, kb['uz'].phones],
        ]
    },
    ru: {
        phoneNumber: [
            [{
                text: kb['ru'].phoneNumber,
                request_contact: true
            }], 
            [kb['ru'].back]
        ],
        question: [
            [kb['ru'].question.yes, kb['ru'].question.no],
        ],
        statistics: [
            [kb['ru'].statistics, kb['ru'].phones],
        ]
    }
}