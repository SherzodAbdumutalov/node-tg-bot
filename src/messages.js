module.exports = {
    greating: `<b>Привет! Начнём?</b> 

<b>Salom! Boshladikmi?</b>`,
    uz: {
        inviting: `Kasting kunlari: 1-2 sentyabr.
Vaqt: 15:00 - 21:00
Maxsus talablar: chiroyli ko'rinish va yaxshi kayfiyat.


Kelajakda siz bilan hamkorlik qilish va bog'lanish uchun ma'lumotlaringizni bazamiziga kiritamiz.

Ustiga bosing «Raqamni yuboring 📲»`,
        choose_language: 'Tilni tanlang!',
        address: `<b>Bizgacha qanday etib kelish mumkin?</b> 

<b>Yandex taksi: </b>Tepamasjid 1
        
<b>Jamoat transporti :</b>
50, 119, 29 raqamli avtobuslar "Do'kon" avtobus bekati 
<b>Eng yaqin metro bekati</b> Buyuk Ipak Yuli 

<b>Qo'shimcha ma'lumot:</b>
Ikki qavatli, jigarrang bino

O'z orzularingizni ro'yobga chiqarish imkoniyatini boy bermang ⭐️`,
        bye: 'Imkoniyatni boy berasiz.😎',
        will_come: 'Ishtirok etish uchun kelasizmi? 😊'

    },
    ru: {
        inviting: `Дата кастинга:  1-2 сентября.
Время : 15:00 - 21:00
Особые требования : быть ухоженным, иметь с собой хорошее настроение.


Ваши данные мы внесём в базу данных, чтобы в дальнейшем связываться с вами для сотрудничества.

Нажмите «Отправить номер 📲»`,
        choose_language: 'Выберите язык!',
        address: `<b>Как до нас добраться? </b>

<b>Для Яндекс такси: </b>тепамасжид 1 
<b>Общественный транспорт:</b> 
Автобусная остановка do’kon под номерами 50 , 119, 29
<b>Ближайшее метро имени</b> Буюк Ипак Йули

<b>Дополнительная информация: </b>
Двухэтажное , коричневое здание .

Не упусти возможность воплотить свою мечту ⭐️`,
        bye: 'Многое теряешь😎',
        will_come: 'Придёте поучаствовать? 😊'

    }
}